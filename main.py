#!/usr/bin/env python
# coding: utf-8

#Copy rights and Owned by robert Chiu
from datetime import datetime
#Importing for midi files
import numpy as np
import midi_utils as midi
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense, Dropout,Activation
from tensorflow.keras.callbacks import ModelCheckpoint

#Prepare Sequences from notes
def notesToSequence(notes, unique, Sequence_length):
    '''
        The Method Convert Notes in to sequences
        notes = array of Notes
        unique = set of all possible notes
        sequence_lengh = length of single sequence of notes, input in LSTM
        The output of the LSTM cell is single Value, which is the proble next note give the sequence of notes
        returns inputs, normalized_inputs and outputs
    '''
    sequence_length = 100
    note_to_int = dict((note, number) for number, note in enumerate(unique))
    network_input = []
    network_output = []
    #It Combines notes of all songs into single array
    notes = np.concatenate(notes)
    for i in range(0, len(notes) - sequence_length, 1):
            sequence_in = notes[i:i + sequence_length]
            sequence_out = notes[i + sequence_length]
            network_input.append([note_to_int[char] for char in sequence_in])
            network_output.append(note_to_int[sequence_out])
    n_patterns = len(network_input)
    network_input = np.reshape(network_input, (n_patterns, sequence_length, 1))
    normalized_input = network_input / float(len(unique))
    network_output=to_categorical(network_output)
    return network_input, normalized_input, network_output

def test(test_file, unique_file = 'set_of_notes', sequence_length =100, load_path='weights.hdf5'):
    '''
        test_file = path of training file
        unique_file= path of set of notes
        sequence_length = length of inputs should be same
        load_path = if further training the model
    '''
    notes = midi.readNotes(test_file)
    unique = midi.readNotes(unique_file)
    inputs, norm_inputs, outputs = notesToSequence(notes,unique,sequence_length)
    model = createModel(inputs, unique, load_path)
    loss = model.test_on_batch(norm_inputs, outputs,)

def createModel(network_input, unique, load_path=None):
    '''
        The Method Creates the Network
        network_input = input array with dimension (batch, sequence_len, val_len)
        give path if weights to load, e.g path = folder/weights.hdf5
        returns the model
    '''
    print("Creating Modle ...")
    model = Sequential()
    model.add(LSTM(
        512,
        input_shape=(network_input.shape[1], network_input.shape[2]),
        return_sequences=True
    ))
    model.add(Dropout(0.3))
    model.add(LSTM(512))
    model.add(Dropout(0.3))
    model.add(Dense(len(unique)))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop',metrics=['accuracy'])
    if (load_path != None):
        model.load_weights(load_path)
    return model

def trainModel(model, network_input, network_output, epoch, batch, save_path):
    '''
    The Method trains the Input Model and save its weights
    model = Keras Model
    network_input = input array with dimensions (batch, sequence_len, val_len)
    network_output = output array with dimensions (batch, set_of_all_notes_len)
    
    '''
    print("training model ...")
    print("beginning:",datetime.now())
    checkpoint = ModelCheckpoint(
        save_path,
        monitor='loss',
        verbose=0,
        save_best_only=True,
        mode='min'
    )
    callbacks_list = [checkpoint]
    #save weights
    # model.save_weights(save_path.format(epoch=0))
    model.fit(network_input, network_output, epochs=epoch, batch_size=batch, callbacks=callbacks_list)
    model.save('saved_model/my_model.hdf5')
    print("ending:",datetime.now())

def train(train_file, epoch, batch, save_path='weights.hdf5', unique_file = 'set_of_notes', sequence_length =100, load_path=None):
    '''
        train_file = path of training file
        epoch = no of iterations
        batch = no of batches to train simultaneously
        save_path = path of weights to save
        unique_file= path of set of notes
        sequence_length = length of inputs should be same
        load_path = if further training the model
    '''
    notes = midi.readNotes(train_file)
    unique = midi.readNotes(unique_file)
    inputs, norm_inputs, outputs = notesToSequence(notes,unique,sequence_length)
    model = createModel(inputs, unique, load_path)
    trainModel(model, norm_inputs, outputs, epoch, batch, save_path)
def generateNotes(model, network_input, unique, no_of_notes):
    '''
        The Method Generates nodes from the inputs
        model = Keras model
        network_input = input array with dimension (batch, sequence_len, val_len)
        unique = set of all notes
        no_of_notes = number of notes to generate
        return the generated array of notes
    '''
    # No of Notes To Create
    n= 100
    #Select Starting Note from the inputs
    print("network input:", network_input)
    start = np.random.randint(0, len(network_input)-1)
    int_to_note = dict((number, note) for number, note in enumerate(unique))
    
    pattern = network_input[start]
    prediction_output = []
    print('\r', end='')
    # generate n notes note
    for i in range(no_of_notes):
        print(str(i+1)+' notes are Generated', end='')
        print('\r', end='')
        prediction_input = np.reshape(pattern, (1, len(pattern), 1)) / float(len(unique))
        prediction = model.predict(prediction_input, verbose=0)
        index = np.argmax(prediction)
        pattern = np.append(pattern, index)
        result = int_to_note[index]
        prediction_output = np.append(prediction_output, result)
        pattern = pattern[1:len(pattern)]
        
    return prediction_output

def generateMusic(file_path, vacab_path, save_path, weights_path, no_of_notes=100, sequence_length= 100):
    '''
        file_path = user input 
        vocab_path = set_of_notes
        save_path = path for saving generated file
        no_of_notes = n notes to generate
        sequence length = same as input sequence length
    '''
    # notes = midi.midiToNotes(file_path)
    # notes = midi.readNotes("data/train")
    notes = midi.readNotes("save_notes/new1")
    unique = midi.readNotes(vacab_path)
    inputs, norm_inputs, outputs = notesToSequence(notes,unique,sequence_length)
    model = createModel(inputs, unique, weights_path)
    generated = generateNotes(model,inputs,unique,no_of_notes)
    midi.notesToMidi(generated, save_path)

# train('data/train', 1,128, load_path='weights.hdf5')
# print("Testing data....")
# testLoss = test('data/test')
# print("end testing data ...")

# './' current dirctory that contain midi file, it will call midiToNote function.  
# pay attention: line 158 and 159 commented out, you can manually readnote from such as save_notes/new1
# save the new music to ./music3
# 'set-of-note : all possible notes in the music (88 notes in piano), it is tuple
# weight.hdf5 is the tensorflow model built from differnt type, such as singers, jazz, rock, etc
# no_of_notes: how many notes the ./music3 wll generate in
generateMusic('./', 'set_of_notes', './music3', weights_path='weights.hdf5',no_of_notes=20)
