#Copy Rights and Owned By Robert Chiu
import pickle
import glob
# from music21 import converter, instrument, note, chord, stream
import music21
from music21 import converter, instrument, note, chord, duration, stream

def check_float(duration): # this function fix the issue which comes from some note's duration. 
                           # For instance some note has duration like 14/3 or 7/3. 
    if ('/' in duration):
        numerator = float(duration.split('/')[0])
        denominator = float(duration.split('/')[1])
        duration = str(float(numerator/denominator))
    return duration

def note_to_int(note): # converts the note's letter to pitch value which is integer form.
    # source: https://musescore.org/en/plugin-development/note-pitch-values
    # idea: https://github.com/bspaans/python-mingus/blob/master/mingus/core/notes.py
    note_base_name = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
    if ('#-' in note):
        first_letter = note[0]
        base_value = note_base_name.index(first_letter)
        octave = note[3]
        value = base_value + 12*(int(octave)-(-1))
        
    elif ('#' in note): # not totally sure, source: http://www.pianofinders.com/educational/WhatToCallTheKeys1.htm
        first_letter = note[0]
        base_value = note_base_name.index(first_letter)
        octave = note[2]
        value = base_value + 12*(int(octave)-(-1))
        
    elif ('-' in note): 
        first_letter = note[0]
        base_value = note_base_name.index(first_letter)
        octave = note[2]
        value = base_value + 12*(int(octave)-(-1))
        
    else:
        first_letter = note[0]
        base_val = note_base_name.index(first_letter)
        octave = note[1]
        value = base_val + 12*(int(octave)-(-1))
        
    return value

def midiToNotes(path):
    '''
        The Method Converts Midi files and Returns Notes of Each Midi File as array
        e.g path = folder/subfolder/
    '''
    # notes = []
    # single_note = []
    print("converting ..")
    for file in glob.glob(path+"/*.mid*"):
        print(file)       
        midi = converter.parse(file)
        notes_to_parse = None
        parts = music21.instrument.partitionByInstrument(midi)
        instrument_names = []
        try:
            for instrument in parts: # Learn names of instruments.
                # print("instrument", instrument)
                name = (str(instrument).split(' ')[-1])[:-1]
                # print("name ", name)
                instrument_names.append(name)
               
        except TypeError:
            print ('Type is not iterable.')
            return None
                
                # Just take piano part. For the future works, we can use different instrument.
        try:
            piano_index = instrument_names.index('Piano')
            print(instrument_names)
            print('index', piano_index)
        except ValueError:
            print ('%s have not any Piano part' %(file))
            return None        
        
        notes_to_parse = parts.parts[piano_index].recurse()
        print("notes-to-parse",notes_to_parse)
        duration_piano = float(check_float((str(notes_to_parse._getDuration()).split(' ')[-1])[:-1]))

        durations = []
        notes = []
        offsets = []
        temp = []
        for element in notes_to_parse:
            print("element", element)
            if isinstance(element, note.Note): # If it is single note
                print("note")
                # notes.append(note_to_int(str(element.pitch))) # Append note's integer value to "notes" list.
                # print("note", str(element.pitch))
                temp.append(str(element.pitch))
                duration = str(element.duration)[27:-1] 
                durations.append(check_float(duration)) 
                offsets.append(element.offset)

            elif isinstance(element, chord.Chord): # If it is chord
                print("chord")
                # print("note", '.'.join(str(note_to_int(str(n)))
                #                     for n in element.pitches))
                temp.append('.'.join(str(note_to_int(str(n)))
                                    for n in element.pitches))
                duration = str(element.duration)[27:-1]
                durations.append(check_float(duration))
                offsets.append(element.offset)
            else:
                print("noth")
        notes.append(temp)
    return notes