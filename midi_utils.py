#Copy Rights and Owned By Robert Chiu
import pickle
import glob
import music21
from music21 import converter, instrument, note, chord, stream

def check_float(duration): # this function fix the issue which comes from some note's duration. 
                           # For instance some note has duration like 14/3 or 7/3. 
    if ('/' in duration):
        numerator = float(duration.split('/')[0])
        denominator = float(duration.split('/')[1])
        duration = str(float(numerator/denominator))
    return duration

def note_to_int(note): # converts the note's letter to pitch value which is integer form.
    # source: https://musescore.org/en/plugin-development/note-pitch-values
    # idea: https://github.com/bspaans/python-mingus/blob/master/mingus/core/notes.py
    note_base_name = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
    if ('#-' in note):
        first_letter = note[0]
        base_value = note_base_name.index(first_letter)
        octave = note[3]
        value = base_value + 12*(int(octave)-(-1))
        
    elif ('#' in note): # not totally sure, source: http://www.pianofinders.com/educational/WhatToCallTheKeys1.htm
        first_letter = note[0]
        base_value = note_base_name.index(first_letter)
        octave = note[2]
        value = base_value + 12*(int(octave)-(-1))
        
    elif ('-' in note): 
        first_letter = note[0]
        base_value = note_base_name.index(first_letter)
        octave = note[2]
        value = base_value + 12*(int(octave)-(-1))
        
    else:
        first_letter = note[0]
        base_val = note_base_name.index(first_letter)
        octave = note[1]
        value = base_val + 12*(int(octave)-(-1))
        
    return value

def midiToNotes(path):
    '''
        The Method Converts Midi files and Returns Notes of Each Midi File as array
        e.g path = folder/subfolder/
    '''
    # notes = []
    # single_note = []
    print("converting ..")
    for file in glob.glob(path+"/*.mid*"):        
        midi = converter.parse(file)
        notes_to_parse = None
        parts = music21.instrument.partitionByInstrument(midi)
        instrument_names = []
        try:
            for instrument in parts: # Learn names of instruments.
                name = (str(instrument).split(' ')[-1])[:-1]
                instrument_names.append(name)

        except TypeError:
            print ('Type is not iterable.')
            return None
                
                # Just take piano part. For the future works, we can use different instrument.
        try:
            piano_index = instrument_names.index('Piano')
        except ValueError:
            print ('%s have not any Piano part' %(file))
            return None        
        
        notes_to_parse = parts.parts[piano_index].recurse()
        
        duration_piano = float(check_float((str(notes_to_parse._getDuration()).split(' ')[-1])[:-1]))

        durations = []
        notes = []
        offsets = []
        
        for element in notes_to_parse:
            print(element)
            if isinstance(element, note.Note): # If it is single note
                # notes.append(note_to_int(str(element.pitch))) # Append note's integer value to "notes" list.
                print("note")
                notes.append(str(element.pitch))
                duration = str(element.duration)[27:-1] 
                durations.append(check_float(duration)) 
                offsets.append(element.offset)

            elif isinstance(element, chord.Chord): # If it is chord
                print("chord")
                notes.append('.'.join(str(note_to_int(str(n)))
                                    for n in element.pitches))
                duration = str(element.duration)[27:-1]
                durations.append(check_float(duration))
                offsets.append(element.offset)
    return notes
def midiToNotes_1(path):
    '''
        The Method Converts Midi files and Returns Notes of Each Midi File as array
        e.g path = folder/subfolder/
    '''
    notes = []
    single_note = []
    print("converting ..")
    for file in glob.glob(path+"/*.mid*"):
        
        midi = converter.parse(file)
        # print("Parsing %s" % file,end='')
        # print('\r', end='')
        print(file)
        notes_to_parse = None
        try: # file has instrument parts
            s2 = instrument.partitionByInstrument(midi)
            print(s2)
            notes_to_parse = s2.parts[0].recurse() 
        except: # file has notes in a flat structure
            notes_to_parse = midi.flat.notes
        temp = []    
        for element in notes_to_parse:
            if isinstance(element, note.Note):
                temp.append(str(element.pitch))
            elif isinstance(element, chord.Chord):
                temp.append('.'.join(str(n) for n in element.normalOrder))
        notes.append(temp)
    return notes
    
def saveNotes(path, notes):
    '''
        The method Saves the notes as binary files
        e.g path = folder/file_name
    '''
    with open(path, 'wb') as filepath:
        pickle.dump(notes, filepath)
        
def readNotes(path):
    '''
        The method Read the Binary File as notes
        e.g path = folder/file_name
    '''
    with open(path, 'rb') as filepath:
        notes = pickle.load(filepath)
    return notes

def notesToMidi(notes, path):
    '''
    The Method Convert notes Values in midi format
    e.g Path = folder/file_name
    '''
    offset = 0
    output_notes = []
    
    # create note and chord objects based on the values generated by the model
    for pattern in notes:
        # pattern is a chord
        if ('.' in pattern) or pattern.isdigit():
            notes_in_chord = pattern.split('.')
            notes = []
            for current_note in notes_in_chord:
                new_note = note.Note(int(current_note))
                new_note.storedInstrument = instrument.Piano()
                notes.append(new_note)
            new_chord = chord.Chord(notes)
            new_chord.offset = offset
            output_notes.append(new_chord)
        # pattern is a note
        else:
            new_note = note.Note(pattern)
            new_note.offset = offset
            new_note.storedInstrument = instrument.Piano()
            output_notes.append(new_note)
    
        # increase offset each iteration so that notes do not stack
        offset += 0.5
    
    midi_stream = stream.Stream(output_notes)
    
    midi_stream.write('midi', fp=path+'.mid')